
#ifndef PROPMNGR_H
#define PROPMNGR_H

#include "RepPropertyData.h"

namespace ElementModel
{
class PropertyManager
{
public:
  void PrepareElementProperties(eRepElementType typ, RepProperties& props);
private:

  void CleanProperties(RepProperties& props);
  void PagePropertiesPrepare(RepProperties& props);
  void LinePropertiesPrepare(RepProperties& props);
  void TextPropertiesPrepare(RepProperties& props);
  void ImagePropertiesPrepare(RepProperties& props);
  void RectPropertiesPrepare(RepProperties& props);

  void SizeScalePropertiesPrepare(RepProperties& props);
  void VisibilityPropertiesPrepare(RepProperties& props);
  void PointPropertiesPrepare(RepProperties& props,bool is_lefttop=true);
  void SizePropertiesPrepare(RepProperties& props, bool is_enabled=true);
  void PaperSizePropertyPrepare(RepProperties& props);
  void FileSelectorPrepare(RepProperties& props);
  void ImgFileSelectorPrepare(RepProperties& props);
  void PenPropertiesPrepare(RepProperties& props);
  void TextColorPropertiesPrepare(RepProperties& props);
  void BrushPropertiesPrepare(RepProperties& props);
  void FontPropertiesPrepare(RepProperties& props);
  void LayerPropertiesPrepare(RepProperties& props);
};

}
#endif // PROPMNGR_H

