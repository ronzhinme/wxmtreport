#include "RepProject.h"

#include <wx/zstream.h>
#include <wx/wfstream.h>
#include <wx/ffile.h>
#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/stdpaths.h>
using namespace ElementModel;

RepProj::RepProj()
{
  IsModify = false;
  ProjFile = wxEmptyString;
}

void RepProj::Clear()
{
  selected_item=-1;
  vars.Clear();
  docs.Clear();
}

bool RepProj::CompressData(wxString data, wxString filename)
{
  int data_len = data.length();
  unsigned char* out_buff = new unsigned char[data_len*2];
  unsigned char* in_buff = new unsigned char[data_len];
  memcpy(in_buff,data.c_str().AsChar(),data_len);

  z_stream strm;
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  int ret=deflateInit(&strm, Z_BEST_COMPRESSION);
  if (ret != Z_OK)
  {
    return false;
  }

  strm.avail_out = data_len*2;
  strm.next_out = out_buff;
  strm.avail_in = data_len;
  strm.next_in = in_buff;
  deflate(&strm, Z_FINISH);
  deflateEnd(&strm);

  wxFile fil;
  if(!fil.Create(filename,true)) {return false;}
  fil.Write(out_buff,data_len);
  fil.Close();

  delete []out_buff;
  delete []in_buff;

  return true;
}

bool RepProj::DecompressData(wxString& data, wxString filename)
{
  wxFile fil;
  if(!fil.Open(filename)) { return false; }

  int data_len = fil.Length();
  unsigned char* out_buff = new unsigned char[data_len*2];
  unsigned char* in_buff = new unsigned char[data_len];

  fil.Read(in_buff,data_len);

  z_stream strm;
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  int ret=inflateInit(&strm);
  if (ret != Z_OK)
  {
    return false;
  }

  strm.avail_in=data_len;
  strm.next_in=in_buff;
  strm.avail_out=data_len*2;
  strm.next_out=out_buff;

  inflate(&strm,Z_FINISH);
  inflateEnd(&strm);

  data = out_buff;

  delete []out_buff;
  delete []in_buff;

  return true;
}

bool RepProj::CompressFile(wxString filename,bool is_compress, wxString& filedata)
{
  if(is_compress)
  {
    return CompressData(filedata,filename);
  }
  else
  {
    return DecompressData(filedata,filename);
  }
  return false;
}

//=== Save Project ===
bool RepProj::SaveProject(wxString filename)
{
  if(filename.IsNull()||filename.IsEmpty()) { return false; }
  wxString filedata;
  SaveSel(filedata);
  filedata+=REPSECT_START;
  filedata+=docs.ToString();
  SaveVars(filedata);
  filedata+=(REPSECT_STOP);
  IsModify = false;
  return CompressFile(filename,true,filedata);
}
void RepProj::SaveVars(wxString& filedata)
{
  filedata+=VARSECT_START;

  wxVector<Var> v = vars.GetVars();
  wxVector<Var>::iterator iter;

  for(iter=v.begin(); iter!=v.end(); iter++)
  {
    if(!(*iter).name.IsSameAs(wxEmptyString))
    {
      filedata+=(*iter).name;
      if(iter!=v.end()-1) { filedata+=_T("\n"); }
    }
  }
  filedata+=VARSECT_STOP;
}
void RepProj::SaveSel(wxString& filedata)
{
  filedata+=SELISEC_START;
  filedata+=wxString::FromDouble(selected_item);
  filedata+=SELISEC_STOP;
}
//Save Project


//=== Load Project ===
bool RepProj::LoadProject(wxString filename)
{
  #ifdef LOG
  wxString msg;
  msg.Printf("Load file: %s \n",filename);
  WRITELOG(msg);
  #endif // LOG

  wxString filedata;
  if(!CompressFile(filename, false, filedata)) { return false; }

  LoadSel(filedata);

  docs.Clear();
  docs.FromString(filedata);

  LoadVars(filedata);

  IsModify = false;
  ProjFile = filename;
  return true;
}
void RepProj::LoadVars(wxString& filedata)
{
  vars.Clear();
  wxString section = GetSectionValue(filedata,VARSECT_START,VARSECT_STOP);
  vars.SetVars(section);
}
void RepProj::LoadSel(wxString& filedata)
{
  wxString section = GetSectionValue(filedata,SELISEC_START,SELISEC_STOP);
  section.ToLong((long*)&selected_item);
}
//Load Project
