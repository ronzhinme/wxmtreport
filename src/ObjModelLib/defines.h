
#ifndef MTDEFINES_H
#define MTDEFINES_H

#ifdef LOG
#include <wx/wfstream.h>
#include <wx/any.h>
#include <wx/stdpaths.h>
#endif

enum eRepElementType
{
  EL_NULL=-1,
  EL_UNKNOWN=0,
  EL_LINE,
  EL_RECT,
  EL_TEXT,
  EL_IMAGE,
  EL_PAGE=255 //
};
enum ePropertyType
{
  PR_STRING=0,
  PR_INTEGER,
  PR_FLOAT,
  PR_SWITCH,
  PR_BOOL,
  PR_BOOLARRAY,
  PR_COLOR,
  PR_FILESELECTOR,
  PR_IMGFILESELECTOR,
  PR_FONTSELECTOR,
  PR_LINETYPE,
  PR_BRUSHTYPE,
};

enum Property_ID
{
  PROP_UNKNOWN_ITEM_ID=0,
  PROP_UNKNOWN_PAGE_ID,
  PROP_NAME_ID   ,
  PROP_TYPE_ID   ,
  PROP_POSTOP_ID ,
  PROP_POSLEFT_ID,
  PROP_POSBOTTOM_ID ,
  PROP_POSRIGHT_ID  ,
  PROP_FILLTYP_ID   ,
  PROP_FILLCLR_ID   ,
  PROP_HEIGHT_ID    ,
  PROP_WIDTH_ID     ,
  PROP_FONT_ID      ,
  PROP_PENTYP_ID    ,
  PROP_PENCLR_ID    ,
  PROP_PENWEIGHT_ID ,
  PROP_FILE_ID      ,
  PROP_IMGFILE_ID   ,
  PROP_TEXT_ID      ,
  PROP_TEXTWRAP_ID  ,
  PROP_TEXTANGLE_ID ,
  PROP_VISIBLE_ID   ,
  PROP_SIZEMETER_ID ,
  PROP_PAGESIZE_ID,
  PROP_PAGEORIENT_ID,
  PROP_VARSLIST_ID,
  PROP_LAYER_ID,

  PGPAGE_PROP_ID,
  PGPAGE_VARS_ID,

  PCAT_COMMON_ID,
  PCAT_POSITION_ID,
  PCAT_FILL_ID     ,
  PCAT_SIZE_ID    ,
  PCAT_FONT_ID     ,
  PCAT_PEN_ID      ,
  PCAT_FILE_ID     ,

  SWITCH_LIST_ID,
  VISIBLE_METHOD_ID,
  REPORT_ELEMENT_ID,
  SIZESCALE_LABEL_ID,
  SIZESCALE_VALUE_ID,

  PAPERSIZE_LABEL_ID,
  PAPERSIZE_VALUE_ID,
  PAPERORIENT_LABEL_ID,
  PAPERORIENT_VALUE_ID,
  PAPER_BGROUND_ID,
  PENTYPE_LABEL_ID,
  BRUSHTYPE_LABEL_ID,
  MAX_ID,
};
#define AUTHOR _("Ronzhin Mikhail")
#define DEFAULT_BGROUND_COLOUR 0x660033
#define BBTN_ADD_HINT _("Add New")
#define BBTN_DEL_HINT _("Del selected")

#define ERRR_FILE_PATH _T("/img/Error.bmp")
#define ROOT_FILE_PATH _T("/img/Root.bmp")
#define PAGE_FILE_PATH _T("/img/Page.bmp")
#define RECT_FILE_PATH _T("/img/Rect.bmp")
#define IMAG_FILE_PATH _T("/img/Image.bmp")
#define LINE_FILE_PATH _T("/img/Line.bmp")
#define TEXT_FILE_PATH _T("/img/Text.bmp")
#define ADDB_FILE_PATH _T("/img/AddBtn.bmp")
#define DELB_FILE_PATH _T("/img/DelBtn.bmp")
#define ICON_FILE_PATH _T("/img/lightReport.ico")

#define PROP_UNKNOWN_ITEM    _("New Element")
#define PROP_UNKNOWN_PAGE    _("New Page")
#define PROP_NAME       _("Name")
#define PROP_TYPE       _("Type");
#define PROP_POSTOP     _("Top");
#define PROP_POSLEFT    _("Left");
#define PROP_POSBOTTOM  _("Bottom");
#define PROP_POSRIGHT   _("Right");
#define PROP_FILLTYP    _("FillType");
#define PROP_FILLCLR    _("FillColour");
#define PROP_HEIGHT     _("Height");
#define PROP_WIDTH      _("Width");
#define PROP_FONT       _("Font");
#define PROP_PENTYP     _("LineType");
#define PROP_PENCLR     _("LineColour");
#define PROP_PENWEIGHT  _("Weight");
#define PROP_FILE       _("File");
#define PROP_IMGFILE    _("Image");
#define PROP_TEXT       _("Text");
#define PROP_TEXTWRAP   _("TextWrap");
#define PROP_VISIBLE    _("Visibility");
#define PROP_SIZEMETER  _("SizeScale");
#define PROP_PAGESIZE   _("PageFormat");
#define PROP_PAGEORIENT _("PageOrientation");
#define PROP_VARSLIST   _("VarList")
#define PROP_TEXTANGLE  _("TextAngle")
#define PROP_BGROUND    _("Back Colour")
#define PGPAGE_PROP     _("Properties")
#define PGPAGE_VARS     _("Variables")
#define PCAT_COMMON     _(" Common")
#define PCAT_POSITION   _(" Position")
#define PCAT_FILL       _(" Fill")
#define PCAT_SIZE       _(" Size")
#define PCAT_FONT       _(" Font")
#define PCAT_PEN        _(" Pen")
#define PCAT_FILE       _(" File")
#define PROP_LAYER      _(" Layer")
#define VISIBLE_METHOD  _(_("Model")+CHOISES_DELIMETR+_("Report"))
#define REPORT_ELEMENT  _(_("None")+CHOISES_DELIMETR+_("Line")+CHOISES_DELIMETR+_("Rect")+CHOISES_DELIMETR+_("Text")+CHOISES_DELIMETR+_("Image"))
#define SIZESCALE_LABEL _T("1/10mm"CHOISES_DELIMETR"mm"CHOISES_DELIMETR"cm")
#define SIZESCALE_VALUE _T("1"CHOISES_DELIMETR"10"CHOISES_DELIMETR"100")
#define PENTYPE_LABEL _( _("Solid")+CHOISES_DELIMETR+\
                        _("Dotted")+CHOISES_DELIMETR+\
                        _("Long dash")+CHOISES_DELIMETR+\
                        _("Short dash")+CHOISES_DELIMETR+\
                        _("Dot and dash")+CHOISES_DELIMETR+\
                        _("Custom dash")+CHOISES_DELIMETR+\
                        _("Transparent")+CHOISES_DELIMETR+\
                        _("Striple")+CHOISES_DELIMETR+\
                        _("Back diagonal hatch")+CHOISES_DELIMETR+\
                        _("Cross diagonal hatch")+CHOISES_DELIMETR+\
                        _("Forward diagonal hatch")+CHOISES_DELIMETR+\
                        _("Cross hatch")+CHOISES_DELIMETR+\
                        _("Horizontal hatch")+CHOISES_DELIMETR+\
                        _("Vertical hatch")\
                        )

#define BRUSHTYPE_LABEL _( _("Solid")+CHOISES_DELIMETR+\
                        _("Transparent")+CHOISES_DELIMETR+\
                        _("Striple")+CHOISES_DELIMETR+\
                        _("Back diagonal hatch")+CHOISES_DELIMETR+\
                        _("Cross diagonal hatch")+CHOISES_DELIMETR+\
                        _("Forward diagonal hatch")+CHOISES_DELIMETR+\
                        _("Cross hatch")+CHOISES_DELIMETR+\
                        _("Horizontal hatch")+CHOISES_DELIMETR+\
                        _("Vertical hatch")\
                        )
#define DEFAULT_PAPERSIZE wxPAPER_A4
#define DEFAULT_PAPERORIENT wxPORTRAIT
#define PAPERSIZE_LABEL _T("A4"CHOISES_DELIMETR"A0");
#define PAPERSIZE_VALUE _T("3"CHOISES_DELIMETR"117");
#define PAPERORIENT_LABEL _(_("Portrait")+CHOISES_DELIMETR+_("Landscape"));
#define PAPERORIENT_VALUE _T("1"CHOISES_DELIMETR"2");

//Project file structure
#define FIELD_DELIMETR _T("\1")
#define CHOISES_DELIMETR _T("\2")
#define PROPERTY_DELIMETR _T("\3")

#define REPPROJ	_T("MTReportProject>")
#define VARSECT _T("GlobVar>")
#define DOCSECT _T("Doc>")
#define DOCESECT _T("DocElem>")
#define SELITEM _T("SelItem>")
#define ITEMID  _T("ItemID>")
#define SECTSTART _T("<")
#define SECTSTOP _T("</")

#define SELISEC_START   SECTSTART SELITEM
#define SELISEC_STOP    SECTSTOP SELITEM
#define ITEMIDSECT_START SECTSTART ITEMID
#define ITEMIDSECT_STOP  SECTSTOP ITEMID
#define VARSECT_START		SECTSTART VARSECT
#define VARSECT_STOP 		SECTSTOP 	VARSECT
#define DOCSECT_START		SECTSTART DOCSECT
#define DOCSECT_STOP 		SECTSTOP 	DOCSECT
#define DOCESECT_START	SECTSTART DOCESECT
#define DOCESECT_STOP 	SECTSTOP 	DOCESECT
#define REPSECT_START		SECTSTART REPPROJ
#define REPSECT_STOP 		SECTSTOP 	REPPROJ
#define PROPSECT_START _T("<Property _=\"")
#define PROPSECT_STOP _T("\"/>")

//mrp -- MTechReportPacket
#define MTREP_PROJEXT _T(".mrp")
#define MTREP_PROJFILTER  _T("lightReportFile (*.mrp)|*.mrp")

#define MAINMENU_FILE _("File")
#define MAINMENU_TOOLS _("Tools")
#define MAINMENU_HELP _("Help")

#define MAINMENU_LOAD _("Load...")
#define MAINMENU_SAVE _("Save...")
#define MAINMENU_PROJECT  _("Project")
#define MAINMENU_PRINT    _("Print...")
#define MAINMENU_QUIT     _("Quit")
#define MAINMENU_SETTINGS _("Settings")
#define MAINMENU_ABOUT_S  _("About")
#define MAINMENU_ABOUT    MAINMENU_ABOUT_S +_T("\tF1")

#define EMPTY_PROJECT_NAME _("New Project")
#define FILE_LOADED_HEAD_HINT   _("Opened: ")
#define NO_FILE_FOUND_HINT      _("No file found")

#define WRITELOG(msg)\
{\
  wxAny x(msg);\
  if(x.CheckType<wxString>())    {\
  wxString path = wxStandardPaths::Get().GetTempDir(); \
  wxFile f(path+"/ligthreport.log",wxFile::write_append);\
  wxFileOutputStream flstream(f);\
  flstream.SeekO(0,wxFromEnd);\
  flstream.Write(msg,msg.Length());\
  flstream.Close(); }\
}
#endif //MTDEFINES_H

