
#include <wx/wx.h>
#include <wx/event.h>
#include <wx/variant.h>
#include "RepPane.h"

wxDEFINE_EVENT(CHPROPVALUE_EVENT, wxCommandEvent);
wxDEFINE_EVENT(SELELEMENT_EVENT, wxCommandEvent);
wxBEGIN_EVENT_TABLE(RepPane,wxScrolledWindow)
EVT_LEFT_DOWN(RepPane::OnMouseLBtnDown)
EVT_LEFT_UP(RepPane::OnMouseLBtnUp)
EVT_MOTION(RepPane::OnMouseDrag)
wxEND_EVENT_TABLE()

RepPane::RepPane(wxWindow* parent,
                 wxWindowID winid,
                 const wxPoint& pos,
                 const wxSize& size,
                 const wxString& name):wxScrolledWindow(parent, winid, pos, size, wxScrolledWindowStyle, name)
{
  wxColour backcolour(DEFAULT_BGROUND_COLOUR);
  this->SetBackgroundColour(backcolour);
  repproj=NULL;
  cur_doc_=NULL;
  cur_el_=NULL;
  selpoint_=NULL;
  mousestate_=maMove;
}

long RepPane::GetSizeScale()
{
  if(cur_doc_==NULL) { return -1; }
  PropertyData* pd = cur_doc_->prop.GetProperty(PROP_SIZEMETER_ID);
  if(pd==NULL) {return -1;}
  return pd->value.GetLong();
}
void RepPane::ChangePositionPropertyValue()
{
  if(cur_el_==NULL) { return; }
  RepProperties* props = &cur_el_->prop;
  long sizescale = GetSizeScale();

  PropertyData* bpoint = props->GetProperty(PROP_POSBOTTOM_ID);
  PropertyData* rpoint = props->GetProperty(PROP_POSRIGHT_ID);
  PropertyData* tpoint = props->GetProperty(PROP_POSTOP_ID);
  PropertyData* lpoint = props->GetProperty(PROP_POSLEFT_ID);
  PropertyData* height = props->GetProperty(PROP_HEIGHT_ID);
  PropertyData* width  = props->GetProperty(PROP_WIDTH_ID);

  if(pointDirection_ == (wxBOTTOM | wxRIGHT))
  {
    if(!bpoint || !rpoint)
    {
      if(!tpoint || !lpoint || !height || !width) { return; }
      width->value =  curmousepos_.x/sizescale - lpoint->value.GetDouble()/sizescale;
      height->value = curmousepos_.y/sizescale - tpoint->value.GetDouble()/sizescale;
    }
    else
    {
      rpoint->value = (long)curmousepos_.x/sizescale;
      bpoint->value = (long)curmousepos_.y/sizescale;
    }
  }
  if(pointDirection_ == (wxTOP | wxLEFT))
  {
    if(!tpoint || !lpoint) { return; }
    if(!bpoint || !rpoint)
    {
      if(!height || !width) { return; }
      width->value =  width->value.GetLong() - (curmousepos_.x/sizescale - lpoint->value.GetLong());
      height->value = height->value.GetLong() - (curmousepos_.y/sizescale - tpoint->value.GetLong());
    }

    lpoint->value = (long)curmousepos_.x/sizescale;
    tpoint->value = (long)curmousepos_.y/sizescale;
  }
  if(pointDirection_ == wxCENTER)
  {
    long diffx;
    long diffy;

    diffx = (curmousepos_.x - mouse_lbtn_click_pos_.x)/sizescale;
    diffy = (curmousepos_.y - mouse_lbtn_click_pos_.y)/sizescale;

    if(lpoint)
    {
      lpoint->value = lpoint->value.GetLong() + diffx;
    }
    if(rpoint)
    {
      rpoint->value = rpoint->value.GetLong() + diffx;
    }
    if(tpoint)
    {
      tpoint->value = tpoint->value.GetLong() + diffy;
    }
    if(bpoint)
    {
      bpoint->value = bpoint->value.GetLong() + diffy;
    }
    mouse_lbtn_click_pos_ = curmousepos_;
  }

  wxCommandEvent event(CHPROPVALUE_EVENT, GetId());
  ProcessWindowEvent(event);
}
void RepPane::OnMouseDrag(wxMouseEvent& evt)
{
  if(evt.Dragging())
  {
    wxClientDC dc(this);
    DoPrepareDC(dc);
    dc.SetMapMode(wxMM_LOMETRIC);
    curmousepos_ = evt.GetLogicalPosition(dc);
    if(CheckDrag())
    {
      mousestate_=maDrag;
    }
    if(selpoint_)
    {
      ChangePositionPropertyValue();
    }
  }
}
bool RepPane::CheckDrag() const
{
  if(abs(mouse_lbtn_click_pos_.x-curmousepos_.x)>10 || abs(mouse_lbtn_click_pos_.y-curmousepos_.y)>10)
  {
    return true;
  }
  return false;
}
void RepPane::OnMouseLBtnDown(wxMouseEvent& evt)
{
  mousestate_=maMove;
  wxClientDC dc(this);
  DoPrepareDC(dc);
  dc.SetMapMode(wxMM_LOMETRIC);
  mouse_lbtn_click_pos_ = evt.GetLogicalPosition(dc);

  GetSelPointAndDirection(cur_el_);
}
void RepPane::GetSelPointAndDirection(Element* el)
{
  if(el==NULL) { return; }

  wxPoint lt;
  wxPoint rb;
  GetTopDownPoints(el, lt,rb);

  RepProperties* props = &el->prop;
  if(!props) { return; }
  if((abs(mouse_lbtn_click_pos_.x - lt.x)<10)&&(abs(mouse_lbtn_click_pos_.y-lt.y)<10))
  {
    selpoint_ = &lt;
    pointDirection_ = (wxTOP | wxLEFT);
  }
  else if((abs(mouse_lbtn_click_pos_.x - rb.x)<10)&&(abs(mouse_lbtn_click_pos_.y-rb.y)<10))
  {
    selpoint_ = &rb;
    pointDirection_ = (wxBOTTOM | wxRIGHT);
  }
  else if(IsInElementRegion(mouse_lbtn_click_pos_, el))
  {
    selpoint_ = &mouse_lbtn_click_pos_;
    pointDirection_= wxCENTER;
  }
}
void RepPane::OnMouseLBtnUp(wxMouseEvent& evt)
{
  if(mousestate_==maMove)
  {
    FindAndSelectElement();
    return;
  }
  if(!selpoint_) { return; }
  selpoint_=NULL;
}
void RepPane::FindAndSelectElement()
{
  if(cur_doc_== NULL)
  {
    return;
  }
  for(int i =0; i<cur_doc_->elems.GetCount(); ++i)
  {
    Element* el = cur_doc_->elems.Get(i);
    if(el==NULL)
    {
      continue;
    }
    if(el!=cur_el_ && IsInElementRegion(mouse_lbtn_click_pos_, el))
    {
      SelectElement(el);
      break;
    }
  }
}
bool RepPane::IsInElementRegion(wxPoint& pos, Element* el)
{
  wxPoint lt;
  wxPoint rb;
  GetTopDownPoints(el, lt,rb);
  wxRect region(lt,rb);
  return region.Contains(pos);
}
void RepPane::SelectElement(Element* el)
{
  wxCommandEvent event(SELELEMENT_EVENT, GetId());
  event.SetClientData(el);
  ProcessWindowEvent(event);
}
void RepPane::DrawSelectedElementMarkers(wxDC* dc)
{
  if(dc==NULL)
  {
    return;
  }

  wxPoint clt;
  wxPoint crb;
  GetTopDownPoints(cur_el_, clt, crb);

  dc->SetPen(*wxRED);
  dc->SetBrush(*wxGREEN);

  int sz = 10;
  wxSize lsz(sz*2,sz*2);
  wxPoint lt(clt.x-sz,clt.y-sz);
  wxPoint rb(crb.x-sz,crb.y-sz);
  dc->DrawRectangle(lt,lsz);
  dc->DrawRectangle(rb,lsz);

  dc->SetBrush(*wxTRANSPARENT_BRUSH);
  wxSize rsz(crb.x - clt.x,crb.y - clt.y);
  dc->DrawRectangle(clt, rsz);
}

void RepPane::GetTopDownPoints(Element* el, wxPoint& lt, wxPoint& rb)
{
  if(el==NULL) { return; }
  RepProperties* props = &el->prop;
  if(props==NULL) { return; }
  long sizescale = GetSizeScale();

  PropertyData* tpoint = props->GetProperty(PROP_POSTOP_ID);
  PropertyData* lpoint = props->GetProperty(PROP_POSLEFT_ID);

  if(!tpoint || !lpoint) { return; }

  lt.x=lpoint->value.GetDouble()*sizescale;
  lt.y=tpoint->value.GetDouble()*sizescale;

  PropertyData* bpoint = props->GetProperty(PROP_POSBOTTOM_ID);
  PropertyData* rpoint = props->GetProperty(PROP_POSRIGHT_ID);
  if(!bpoint || !rpoint)
  {
    PropertyData* height = props->GetProperty(PROP_HEIGHT_ID);
    PropertyData* width = props->GetProperty(PROP_WIDTH_ID);
    if(!height || !width) { return; }

    rb.x = lpoint->value.GetDouble()*sizescale + width->value.GetDouble()*sizescale;
    rb.y = tpoint->value.GetDouble()*sizescale + height->value.GetDouble()*sizescale;
  }
  else
  {
    rb.x = rpoint->value.GetDouble()*sizescale;
    rb.y = bpoint->value.GetDouble()*sizescale;
  }
}

void RepPane::OnDraw(wxDC& dc)
{
  Elements<Element>* elems=NULL;
  dc.Clear();
  dc.SetMapMode(wxMM_LOMETRIC);

  if(cur_doc_==NULL) { return; }
  elems = &cur_doc_->elems;
  long vis =cur_doc_->prop.GetProperty(PROP_VISIBLE_ID)->value.GetLong()&MODEL;
  if(vis==0)
  {
    return;
  }

  RepPrinter* p = new RepPrinter(MODEL,&dc);
  if(p->SetRepProjectObject(repproj, cur_doc_->id)) { p->PrintDocument(); }
  delete p;

  DrawSelectedElementMarkers(&dc);
  SetVirtualSize(dc.LogicalToDeviceXRel(dc.MaxX()),dc.LogicalToDeviceYRel(dc.MaxY()));
  SetScrollRate(5,5);
}
void RepPane::Repaint()
{
  wxClientDC dc(this);
  DoPrepareDC(dc);
  OnDraw(dc);
}
void RepPane::ShowElementMarkers(long val)
{
  if(cur_doc_==NULL || cur_doc_->id == val)
  {
    cur_el_=NULL;
    return;
  }

  cur_el_ = cur_doc_->elems.Get(val,false);
}
void RepPane::ScrollToSelectedElement()
{
  if(cur_el_==NULL)
  {
    return;
  }
  wxClientDC dc(this);
  DoPrepareDC(dc);
  dc.SetMapMode(wxMM_LOMETRIC);

  wxPoint lt;
  wxPoint rb;
  wxPoint lefttop;
  GetTopDownPoints(cur_el_, lt, rb);
  if(lt.x<rb.x)
  {
    lefttop.x = lt.x;
  }
  else
  {
    lefttop.x = rb.x;
  }
  long h;
  if(lt.y<rb.y)
  {
    lefttop.y = lt.y;
  }
  else
  {
    lefttop.y = rb.y;
  }

  wxPoint pxinunit;
  GetScrollPixelsPerUnit(&pxinunit.x,&pxinunit.y);
  long x = dc.LogicalToDeviceXRel( lefttop.x )/5;
  long y = dc.LogicalToDeviceYRel( lefttop.y )/5;
  Scroll(x, y);
}
void RepPane::SetPageId(long val)
{
  cur_doc_ = repproj->docs.Get(val,false);
}
