#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

//(*Headers(SettingsDialog)
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/combobox.h>
//*)

class SettingsDialog: public wxDialog
{
	public:

		SettingsDialog(wxWindow* parent);
		virtual ~SettingsDialog();

		//(*Declarations(SettingsDialog)
		wxButton* Button1;
		wxStaticText* StaticText1;
		wxButton* Button2;
		wxComboBox* cb_language;
		//*)

	protected:

		//(*Identifiers(SettingsDialog)
		static const long ID_STATICTEXT1;
		static const long ID_COMBOBOX1;
		static const long ID_BUTTON1;
		static const long ID_BUTTON2;
		//*)

	private:

		//(*Handlers(SettingsDialog)
		void Oncb_languageSelected(wxCommandEvent& event);
		void OnButtonCancel_Click(wxCommandEvent& event);
		void OnButtonApplyClick(wxCommandEvent& event);
		//*)
    wxPGChoices lang_arr;
    void LanguageSettingInit();
		DECLARE_EVENT_TABLE()
};

#endif
