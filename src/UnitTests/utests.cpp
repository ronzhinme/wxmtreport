#include <tut/tut.hpp>
#include <tut/tut_reporter.hpp>
#include <iostream>
#include <wx/wx.h>

#include "../ObjModelLib/RepProject.h"
#include "../ObjModelLib/defines.h"

using namespace std;
using namespace ElementModel;

namespace tut
{
test_runner_singleton runner;
struct data   // (1)
{
};
typedef test_group<data> tg; // (2)
tg testgroup("wxMTReport_Test");    // (3)
typedef tg::object testobject;


template<>
template<>
void testobject::test<1>()
{
  set_test_name("+/- Doc in Project");

  int cnt = 100;
  RepProj proj;
  ensure_equals("\nInit error",proj.docs.GetCount(),0);
  for(int f=0; f<cnt; ++f)
    {
      proj.docs.AddNew(EL_PAGE);
      ensure_equals("\nError + doc",f+1,proj.docs.GetCount());
    }
  //printf("\nAfter + doc count = %d",proj.docs.GetCount());
  for(int f=cnt; f>0; --f)
    {
      proj.docs.Delete(f-1,true);
      ensure_equals("\nError - doc",f-1,proj.docs.GetCount());
    }
//  printf("\nAfter - doc count = %d",proj.docs.GetCount());
}

void testobject::test<2>()
{
  set_test_name("+/- Element in doc");

  int cnt = 100;
  RepProj proj;
  ensure_equals(proj.docs.GetCount(),0);
  proj.docs.AddNew(EL_PAGE);
  Doc* doc = proj.docs.Get(0,true);
  if(!doc)
    {
      fail("\ndoc==NULL");
    }
  for(int f=0; f<cnt; ++f)
    {
      doc->elems.AddNew(EL_LINE);
      ensure_equals("\nError + element",f+1,doc->elems.GetCount());
    }
//  printf("\nAfter + elems count = %d",doc->elems.GetCount());
  for(int f=cnt; f>0; --f)
    {
      doc->elems.Delete(f-1,true);
      ensure_equals("\nError - element",f-1,doc->elems.GetCount());
    }
//  printf("\nAfter - elems count = %d",doc->elems.GetCount());
}

void testobject::test<3>()
{
  set_test_name("\nFind doc");

  wxString doc_name = _T("TestDoc");
  RepProj proj;
  ensure_equals(proj.docs.GetCount(),0);

  Doc* added_doc = proj.docs.AddNew(EL_PAGE);
  ensure("\nError added_doc",added_doc);
  added_doc->SetName(doc_name);

  Doc* indxdoc = proj.docs.Get(0,true);
  ensure("\nError indxdoc",indxdoc);

  Doc* namedoc = proj.docs.Get(added_doc->GetName(),false);
  ensure("\nError namedoc",namedoc);

  Doc* iddoc = proj.docs.Get(added_doc->id,false);
  ensure("\nError iddoc",iddoc);
}

void testobject::test<4>()
{
  set_test_name("Find element");
  wxString el_name = _T("TestEl");
  RepProj proj;
  ensure_equals(proj.docs.GetCount(),0);

  Doc* added_doc =  proj.docs.AddNew(EL_PAGE);
  ensure("\nError added_doc",added_doc);

  Element* added_el = added_doc->elems.AddNew(EL_LINE);
  ensure("\nError added_el",added_el);
  added_el->SetName(el_name);

  Element* indxel = added_doc->elems.Get(0,true);
  ensure("\nError indxel",indxel);

  Element* nameel = added_doc->elems.Get(added_el->GetName(),false);
  ensure("\nError nameel",nameel);

  Element* idel = added_doc->elems.Get(added_el->id,false);
  ensure("\nError idel",idel);
}

void testobject::test<5>()
{
  set_test_name("Change Doc property");

  wxString new_name = _T("newdocname");
  wxString old_name = wxString(new_name);
  RepProj proj;
  ensure_equals(proj.docs.GetCount(),0);

  Doc* added_doc =  proj.docs.AddNew(EL_PAGE);
  ensure("\nError added_doc",added_doc);

  PropertyData* dt = added_doc->prop.GetProperty(PROP_NAME_ID);
  ensure("\nError. dt=NULL",dt);

  old_name = dt->value.GetString();
  dt->value = new_name;

  ensure("\nError. Name property not set", !new_name.IsSameAs(old_name));
}

void testobject::test<6>()
{
  set_test_name("Change Elem property");

  wxString new_name = _T("newelname");
  wxString old_name = wxString(new_name);
  RepProj proj;
  ensure_equals(proj.docs.GetCount(),0);

  Doc* added_doc =  proj.docs.AddNew(EL_PAGE);
  ensure("\nError added_doc",added_doc);

  Element* added_el = added_doc->elems.AddNew(EL_LINE);
  ensure("\nError added_el",added_el);

  PropertyData* dt = added_el->prop.GetProperty(PROP_NAME_ID);
  ensure("\nError. dt=NULL",dt);

  old_name = dt->value.GetString();
  dt->value = new_name;

  ensure("\nError. Name property not set", !new_name.IsSameAs(old_name));
}

void testobject::test<7>()
{
  set_test_name("+|- vars");
  RepProj proj;
  int var_cnt=100;
  for(int f=0;f<var_cnt;++f)
  {
    proj.vars.AddVar(_T("var")+ wxString::FromCDouble(f));
  }
  ensure_equals("", proj.vars.GetVars().size(),var_cnt);

  for(int f=0;f<var_cnt;++f)
  {
    proj.vars.DelVar(_T("var")+ wxString::FromCDouble(f));
  }
  ensure_equals("", proj.vars.GetVars().size(),0);
}

void testobject::test<9>()
{
  set_test_name("Save project | Load project");

  RepProj proj;
  ensure_equals(proj.docs.GetCount(),0);

  wxString save_file = _T("./save_test.mrp");
  wxString lsave_file = _T("./lsave_test.mrp");

  int doc_cnt = 100;
  int el_cnt = doc_cnt * 1;

  for(int f=0;f<doc_cnt;++f)
  {
    //printf("\n f=%d",f);
    Doc* added_doc = proj.docs.AddNew(EL_PAGE);
    ensure("\nError added_doc",added_doc);

    for(int i=0;i<el_cnt;++i)
    {
      //printf("\n i=%d",i);
      Element* added_el = added_doc->elems.AddNew(EL_LINE);
      ensure("\nError added_el",added_el);
    }
  }


  proj.SaveProject(save_file);

  RepProj p;
  p.LoadProject(save_file);
  p.SaveProject(lsave_file);

  //����� ���� ���������������� ��������� ���� ������. ���� ������ ������ � �������.
}



};

int main()
{
  wxInitialize(); //������������� ���������� wxWidgets

  tut::reporter reporter;
  tut::runner.get().set_callback(&reporter);

  tut::runner.get().run_tests();

  return !reporter.all_ok();
}

