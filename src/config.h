
#include <wx/wx.h>
#include <stdio.h>
struct ConfInfo
{
  unsigned int lang_num;
};

#define CONF_FILE _T("/ligthreport.cfg")

class Conf
{
private:
  wxString conffile_path;

public:
  ConfInfo inf;

  Conf(wxString dir)
  {
    conffile_path = dir + CONF_FILE;
    inf.lang_num=wxLANGUAGE_DEFAULT;
    LoadInfo();
  }

  ~Conf()
  {
    SaveInfo();
  }

  bool LoadInfo()
  {
    const char* filepath = conffile_path.c_str().AsChar();
    FILE* fil = fopen(filepath,"rb");
    if(fil==NULL) { return false; }
    if(fread(&inf,sizeof(ConfInfo),1,fil)!=sizeof(ConfInfo)) { return false; }
    fclose(fil);
  }

  bool SaveInfo()
  {
    const char* filepath = conffile_path.c_str().AsChar();
    FILE* fil = fopen(filepath,"wb");
    if(fil==NULL) { return false; }
    fwrite(&inf,sizeof(ConfInfo),1,fil);
    fclose(fil);
    return true;
  }
};
