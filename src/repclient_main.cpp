
#ifdef __WXMSW__
#include <conio.h>
#include <windows.h>
#else
#include "wxMTReport_lib.h"
#include <string.h>
#include <unistd.h>
#endif // defined

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
using namespace std;

enum MenuCommands
{
  REP_EXIT=0,
  REP_OPEN,
  REP_PAGESEL,
  REP_VARCNT,
  REP_VARGET,
  REP_VARSET,
  REP_PRINT,
};

#define MENU "\t\tMenu: \n\n\t\
1)Open Report File \n\t\
2)Select page \n\t\
3)Get Report Variables Count\n\t\
4)Get Report Variable \n\t\
5)Set Report Variable Value \n\t\
6)Print Report \n\t\
0)Exit\n"

#if defined __WXMSW__
bool            (*OpenReport)(const char* filename);
long            (*GetPageCount)();
const char*  (*GetPageName)(long indx);
bool            (*SetCurrentDocument)(long indx);
long            (*GetVarCount)();
const char*  (*GetVarName)(long indx);
const char*  (*GetVarVal)(long indx);
bool            (*SetVarVal)(long indx,char* val);
void            (*PrintPage)();
#endif

long page_cnt=-1;
long page_indx=-1;

#if defined __WXMSW__
bool LoadLib()
{
  HINSTANCE dll = LoadLibrary("lightReportLib.DLL");
  if(dll==NULL)
  {
    return false;
  }
  OpenReport =(bool(*)(const char*))GetProcAddress(dll,"OpenReport");
  if(OpenReport==NULL)
  {
    return false;
  }
  GetPageCount =(long(*)())GetProcAddress(dll,"GetPageCount");
  if(GetPageCount==NULL)
  {
    return false;
  }
  GetPageName =( const char* (*)(long))GetProcAddress(dll,"GetPageName");
  if(GetPageName==NULL)
  {
    return false;
  }
  SetCurrentDocument =( bool (*)(long))GetProcAddress(dll,"SetCurrentDocument");
  if(SetCurrentDocument==NULL)
  {
    return false;
  }
  GetVarCount =( long (*)())GetProcAddress(dll,"GetVarCount");
  if(GetVarCount==NULL)
  {
    return false;
  }
  GetVarName =( const char* (*)(long))GetProcAddress(dll,"GetVarName");
  if(GetVarName==NULL)
  {
    return false;
  }
  GetVarVal =( const char* (*)(long))GetProcAddress(dll,"GetVarVal");
  if(GetVarVal==NULL)
  {
    return false;
  }
  SetVarVal =( bool (*)(long,char*))GetProcAddress(dll,"SetVarVal");
  if(SetVarVal==NULL)
  {
    return false;
  }
  PrintPage =( void (*)())GetProcAddress(dll,"PrintPage");
  if(PrintPage==NULL)
  {
    return false;
  }
  return true;
}
#endif // defined

void LoadProjectFunc()
{
  puts("\nInput project file name");
  string fname;
  while(getc(stdin)!='\n')
  {
  }
  getline(std::cin ,fname);

  if(!OpenReport(fname.c_str()))
  {
    puts("\nProject file is not opened");
  }
  else
  {
    puts("\n\nProject file is opened");
    page_cnt=GetPageCount();
    printf("\nPageCount: %ld ",page_cnt);
    for(int f=0; f<page_cnt; ++f)
    {
      printf("\npage %d: %s",f,GetPageName(f));
    }
  }
}

void ProcessSelection(char ch)
{
  switch(ch)
  {
  case REP_OPEN:
  {
    page_cnt=-1;
    page_indx=-1;
    LoadProjectFunc();
    break;
  }
  case REP_PAGESEL:
  {
    puts("\nSelect page indx");
    page_indx=-1;
    if(scanf("%ld",&page_indx)==EOF)return;
    if(page_indx>page_cnt)
    {
      puts("\nselect is out of range");
    }
    else
    {
      bool res = SetCurrentDocument(page_indx);
      res?puts("\npage selected"):puts("\npage not selected");
    }
    break;
  }
  case REP_VARCNT:
  {
    printf("\nVar count:%ld",GetVarCount());
    break;
  }
  case REP_VARGET:
  {
    puts("\nwhat index var?");
    long c;
    if(scanf("%ld",&c)==EOF)return;

    printf("\nVar indx:%ld name:%s",c,GetVarName(c));
    printf("\nVar indx:%ld val :%s",c,GetVarVal(c));
    break;
  }
  case REP_VARSET:
  {
    puts("\nwhat index var?");
    long c;
    if(scanf("%ld",&c)==EOF)return;

    puts("\nvalue?(255 symbols MAX)");

    char val[255];
    memset(val,0,255);
    if(scanf("%s",val)==EOF)return;

    bool res = SetVarVal(c,val);
    if(res)
    {
      puts("ok!");
    }
    else
    {
      puts("err!");
    }
    break;
  }
  case REP_PRINT:
  {
    PrintPage();
    break;
  }
  }
  if(ch!=REP_EXIT)
  {
    int k=2;
    if(ch==REP_PRINT)k=10;
  #ifdef __WXMSW__
    Sleep(1000*k);
    #else
    sleep(k);
    #endif
  }
}
int main()
{
#ifdef __WXMSW__
  if(!LoadLib())
  {
    return -1;
  }
#endif // __WXMSW__
  while(1)
  {
#ifdef __WXMSW__
    if(system("cls")!=0)return 1;
    SetConsoleCP(1251);
    printf("%d\n",GetConsoleCP());
    system("chcp");
#else
    if(system("clear")!=0)return 1;
#endif // __WXMSW__


    printf(MENU);

    long c=0;
    if(scanf("%ld",&c)==EOF)return -1;

    ProcessSelection(c);
    if(c==REP_EXIT)
    {
      break;
    }
  }
  return 0;
}
