/***************************************************************
 * Name:      wxMTReportApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Ronzhin Mikhail ()
 * Created:   2013-08-22
 * Copyright: Ronzhin Mikhail ()
 * License:
 **************************************************************/

#include "wxMTReportApp.h"
#include <wx/fontdlg.h>
#include <wx/dir.h>
#include <wx/stdpaths.h>
#include <wx/filename.h>
//(*AppHeaders
#include "wxMTReportMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(wxMTReportApp);

bool wxMTReportApp::OnInit()
{
  wxStandardPaths defpath = wxStandardPaths::Get();
  wxString dir = defpath.GetTempDir();

  cnf=new Conf(dir);
  loc=NULL;
  SelectLanguage(cnf->inf.lang_num);

  wxFileName appfile(defpath.GetExecutablePath());
  AppDir = appfile.GetPath();

  //(*AppInitialize
  bool wxsOK = true;
  wxInitAllImageHandlers();
  if ( wxsOK )
  {
  	wxMTReportFrame* Frame = new wxMTReportFrame(0);
  	Frame->Show();
  	SetTopWindow(Frame);
  }
  //*)
  return wxsOK;
}

wxMTReportApp::~wxMTReportApp()
{
  cnf->SaveInfo();
}

void wxMTReportApp::SelectLanguage(int lang)
{
  if(loc!=NULL) { delete loc; }
  loc=new wxLocale(lang);

  wxString name = wxLocale::GetLanguageCanonicalName(lang);
  loc->AddCatalog(name);
}

void GetAvailableLanguages(wxArrayInt* lang_arr)
{
  if(lang_arr==NULL)return;

  for(int f=wxLANGUAGE_DEFAULT+1; f<wxLANGUAGE_USER_DEFINED; ++f)
  {
    if(f==wxLANGUAGE_ENGLISH) { lang_arr->Add(f);continue;}

    wxString name = wxLocale::GetLanguageCanonicalName(f);
    if(name.IsEmpty()) { continue; }

    wxLocale* lc = new wxLocale();
    lc->AddCatalog(name);
    bool added = lc->IsLoaded(name);
    delete lc;

    if(!wxLocale::IsAvailable(f) || !added ) { continue; }
    lang_arr->Add(f);
  }
}

wxPGChoices* wxMTReportApp::GetAvailableLanguageChoises()
{
  wxPGChoices* languages=new wxPGChoices();
  wxArrayInt* langs=new wxArrayInt();
  GetAvailableLanguages(langs);

  for(unsigned int f=0;f<langs->GetCount();++f)
  {
    int lng = langs->Item(f);
    languages->Add(wxLocale::GetLanguageName(lng) ,lng);
  }
  delete langs;
  return languages;
}
