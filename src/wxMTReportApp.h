/***************************************************************
 * Name:      wxMTReportApp.h
 * Purpose:   Defines Application Class
 * Author:    Ronzhin Mikhail ()
 * Created:   2013-08-22
 * Copyright: Ronzhin Mikhail ()
 * License:
 **************************************************************/

#ifndef WXMTREPORTAPP_H
#define WXMTREPORTAPP_H

#include <wx/app.h>

#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/property.h>

#include "config.h"

class wxMTReportApp : public wxApp
{
public:
  virtual bool OnInit();
  void SelectLanguage(int lang);
  wxPGChoices* GetAvailableLanguageChoises();

  Conf *cnf;
  wxString AppDir;
private:
  ~wxMTReportApp();
  wxLocale *loc;
};

DECLARE_APP(wxMTReportApp);

#endif // WXMTREPORTAPP_H
