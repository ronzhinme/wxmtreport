/***************************************************************
 * Name:      wxMTReportMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    Ronzhin Mikhail ()
 * Created:   2013-08-22
 * Copyright: Ronzhin Mikhail ()
 * License:
 **************************************************************/
#include "version_generator.h"
#include "wxMTReportMain.h"
#include "wxMTReportApp.h"
#include "FileDrop.h"
#include "DrawedComboProperty.h"

#include <wx/wx.h>
#include <wx/msgdlg.h>
#include <wx/popupwin.h>
#include <wx/propgrid/advprops.h>
#include <wx/window.h>
#include <wx/choicdlg.h>
#include <wx/cmdline.h>

//(*InternalHeaders(wxMTReportFrame)
#include <wx/artprov.h>
#include <wx/bitmap.h>
#include <wx/icon.h>
#include <wx/intl.h>
#include <wx/image.h>
#include <wx/string.h>
//*)

//(*IdInit(wxMTReportFrame)
const long wxMTReportFrame::ID_REPPANE = wxNewId();
const long wxMTReportFrame::ID_PANEL1 = wxNewId();
const long wxMTReportFrame::ID_BITMAPBUTTON1 = wxNewId();
const long wxMTReportFrame::ID_BITMAPBUTTON2 = wxNewId();
const long wxMTReportFrame::ID_TREECTRL1 = wxNewId();
const long wxMTReportFrame::ID_PANEL3 = wxNewId();
const long wxMTReportFrame::ID_PROPMNGR1 = wxNewId();
const long wxMTReportFrame::ID_PANEL4 = wxNewId();
const long wxMTReportFrame::ID_SPLITTERWINDOW2 = wxNewId();
const long wxMTReportFrame::ID_PANEL2 = wxNewId();
const long wxMTReportFrame::ID_SPLITTERWINDOW1 = wxNewId();
const long wxMTReportFrame::MFP_Load = wxNewId();
const long wxMTReportFrame::MFP_SAVE = wxNewId();
const long wxMTReportFrame::MF_PROJECT = wxNewId();
const long wxMTReportFrame::MF_PRINT = wxNewId();
const long wxMTReportFrame::MF_QUIT = wxNewId();
const long wxMTReportFrame::MFT_SETTINGS = wxNewId();
const long wxMTReportFrame::MFT_VARS = wxNewId();
const long wxMTReportFrame::MH_ABOUT = wxNewId();
const long wxMTReportFrame::ID_STATUSBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(wxMTReportFrame, wxFrame)
  EVT_COMMAND (ID_REPPANE, CHPROPVALUE_EVENT, wxMTReportFrame::onChangePropertyValue)
  EVT_COMMAND (ID_REPPANE, SELELEMENT_EVENT, wxMTReportFrame::onChangeSelectElement)
  //(*EventTable(wxMTReportFrame)
  //*)
END_EVENT_TABLE()

wxMTReportFrame::~wxMTReportFrame()
{
  //(*Destroy(wxMTReportFrame)
  //*)
  delete repproj_;
}
wxMTReportFrame::wxMTReportFrame(wxWindow* parent, wxWindowID id)
{
  //(*Initialize(wxMTReportFrame)
  wxBoxSizer* BoxSizer4;
  wxBoxSizer* BoxSizer6;
  wxBoxSizer* BoxSizer5;
  wxBoxSizer* BoxSizer2;
  wxBoxSizer* BoxSizer1;
  wxBoxSizer* BoxSizer3;

  Create(parent, wxID_ANY, _("Report Model Generator"), wxDefaultPosition, wxDefaultSize, wxCAPTION|wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
  SetClientSize(wxSize(640,480));
  {
  	wxIcon FrameIcon;
  	FrameIcon.CopyFromBitmap(wxBitmap(wxImage(_T(".\\img\\lightReport.png"))));
  	SetIcon(FrameIcon);
  }
  BoxSizer1 = new wxBoxSizer(wxVERTICAL);
  SplitterWindow1 = new wxSplitterWindow(this, ID_SPLITTERWINDOW1, wxDefaultPosition, wxDefaultSize, wxSP_3D|wxSP_NO_XP_THEME, _T("ID_SPLITTERWINDOW1"));
  SplitterWindow1->SetMinSize(wxSize(100,100));
  SplitterWindow1->SetMinimumPaneSize(100);
  SplitterWindow1->SetSashGravity(0.5);
  Panel1 = new wxPanel(SplitterWindow1, ID_PANEL1, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL1"));
  BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
  reppane = new RepPane(Panel1,ID_REPPANE,wxDefaultPosition,wxDefaultSize,_T("ID_REPPANE"));
  BoxSizer3->Add(reppane, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 0);
  Panel1->SetSizer(BoxSizer3);
  BoxSizer3->Fit(Panel1);
  BoxSizer3->SetSizeHints(Panel1);
  Panel2 = new wxPanel(SplitterWindow1, ID_PANEL2, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL2"));
  BoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
  SplitterWindow2 = new wxSplitterWindow(Panel2, ID_SPLITTERWINDOW2, wxDefaultPosition, wxDefaultSize, wxSP_3D|wxSP_NO_XP_THEME, _T("ID_SPLITTERWINDOW2"));
  SplitterWindow2->SetMinSize(wxSize(100,100));
  SplitterWindow2->SetMinimumPaneSize(100);
  SplitterWindow2->SetSashGravity(0.5);
  Panel3 = new wxPanel(SplitterWindow2, ID_PANEL3, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL3"));
  BoxSizer4 = new wxBoxSizer(wxVERTICAL);
  BoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
  BBtn_Add = new wxBitmapButton(Panel3, ID_BITMAPBUTTON1, wxArtProvider::GetBitmap(wxART_MAKE_ART_ID_FROM_STR(_T("wxART_NEW")),wxART_BUTTON), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON1"));
  BoxSizer6->Add(BBtn_Add, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
  BBtn_Del = new wxBitmapButton(Panel3, ID_BITMAPBUTTON2, wxArtProvider::GetBitmap(wxART_MAKE_ART_ID_FROM_STR(_T("wxART_DELETE")),wxART_BUTTON), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON2"));
  BoxSizer6->Add(BBtn_Del, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer4->Add(BoxSizer6, 0, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 0);
  TreeCtrl1 = new wxTreeCtrl(Panel3, ID_TREECTRL1, wxDefaultPosition, wxDefaultSize, wxTR_SINGLE|wxTR_DEFAULT_STYLE, wxDefaultValidator, _T("ID_TREECTRL1"));
  TreeCtrl1->ExpandAll();
  BoxSizer4->Add(TreeCtrl1, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 0);
  Panel3->SetSizer(BoxSizer4);
  BoxSizer4->Fit(Panel3);
  BoxSizer4->SetSizeHints(Panel3);
  Panel4 = new wxPanel(SplitterWindow2, ID_PANEL4, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL4"));
  BoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
  propmngr = new wxPropertyGridManager(Panel4,ID_PROPMNGR1,wxDefaultPosition, wxDefaultSize, wxPG_SPLITTER_AUTO_CENTER|wxPG_TOOLBAR);
  BoxSizer5->Add(propmngr, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 0);
  Panel4->SetSizer(BoxSizer5);
  BoxSizer5->Fit(Panel4);
  BoxSizer5->SetSizeHints(Panel4);
  SplitterWindow2->SplitHorizontally(Panel3, Panel4);
  BoxSizer2->Add(SplitterWindow2, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 0);
  Panel2->SetSizer(BoxSizer2);
  BoxSizer2->Fit(Panel2);
  BoxSizer2->SetSizeHints(Panel2);
  SplitterWindow1->SplitVertically(Panel1, Panel2);
  BoxSizer1->Add(SplitterWindow1, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 0);
  SetSizer(BoxSizer1);
  MenuBar1 = new wxMenuBar();
  Menu1 = new wxMenu();
  MenuItem4 = new wxMenu();
  MenuItem5 = new wxMenuItem(MenuItem4, MFP_Load, _("Load..."), wxEmptyString, wxITEM_NORMAL);
  MenuItem4->Append(MenuItem5);
  MenuItem6 = new wxMenuItem(MenuItem4, MFP_SAVE, _("Save..."), wxEmptyString, wxITEM_NORMAL);
  MenuItem4->Append(MenuItem6);
  Menu1->Append(MF_PROJECT, _("Project"), MenuItem4, wxEmptyString);
  MenuItem3 = new wxMenuItem(Menu1, MF_PRINT, _("Print..."), wxEmptyString, wxITEM_NORMAL);
  Menu1->Append(MenuItem3);
  Menu1->AppendSeparator();
  MenuItem1 = new wxMenuItem(Menu1, MF_QUIT, _("Quit"), wxEmptyString, wxITEM_NORMAL);
  Menu1->Append(MenuItem1);
  MenuBar1->Append(Menu1, _("File"));
  Menu3 = new wxMenu();
  MenuItem7 = new wxMenuItem(Menu3, MFT_SETTINGS, _("Settings"), wxEmptyString, wxITEM_NORMAL);
  Menu3->Append(MenuItem7);
  MenuItem8 = new wxMenuItem(Menu3, MFT_VARS, _("Variables"), wxEmptyString, wxITEM_NORMAL);
  Menu3->Append(MenuItem8);
  MenuBar1->Append(Menu3, _("Tools"));
  Menu2 = new wxMenu();
  MenuItem2 = new wxMenuItem(Menu2, MH_ABOUT, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
  MenuItem2->SetBitmap(wxArtProvider::GetBitmap(wxART_MAKE_ART_ID_FROM_STR(_T("wxART_HELP")),wxART_MENU));
  Menu2->Append(MenuItem2);
  MenuBar1->Append(Menu2, _("Help"));
  SetMenuBar(MenuBar1);
  StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
  int __wxStatusBarWidths_1[1] = { -1 };
  int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
  StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
  StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
  SetStatusBar(StatusBar1);
  SingleInstanceChecker1.Create(wxTheApp->GetAppName() + _T("_") + wxGetUserId() + _T("_Guard"));
  ImageList1 = new wxImageList(16, 16, 1);
  SetSizer(BoxSizer1);
  Layout();
  Center();

  Connect(ID_BITMAPBUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxMTReportFrame::OnButtonAdd);
  Connect(ID_BITMAPBUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxMTReportFrame::OnButtonDel);
  // Set the images for TreeCtrl1.
  TreeCtrl1->SetImageList(ImageList1);
  Connect(ID_TREECTRL1,wxEVT_COMMAND_TREE_BEGIN_DRAG,(wxObjectEventFunction)&wxMTReportFrame::OnTreeCtrl1BeginDrag);
  Connect(ID_TREECTRL1,wxEVT_COMMAND_TREE_END_DRAG,(wxObjectEventFunction)&wxMTReportFrame::OnTreeCtrl1EndDrag);
  Connect(ID_TREECTRL1,wxEVT_COMMAND_TREE_SEL_CHANGED,(wxObjectEventFunction)&wxMTReportFrame::OnTreeCtrlSelectionChanged);
  Connect(MFP_Load,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxMTReportFrame::OnMenuItemLoadProject);
  Connect(MFP_SAVE,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxMTReportFrame::OnMenuItemSaveProject);
  Connect(MF_PRINT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxMTReportFrame::OnMenuItemPrint);
  Connect(MF_QUIT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxMTReportFrame::OnQuit);
  Connect(MFT_SETTINGS,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxMTReportFrame::OnMenuSettingsSelected);
  Connect(MFT_VARS,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxMTReportFrame::OnMenuVariablesSelected);
  Connect(MH_ABOUT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxMTReportFrame::OnAbout);
  Connect(wxID_ANY,wxEVT_CLOSE_WINDOW,(wxObjectEventFunction)&wxMTReportFrame::OnClose);
  //*)
  this->
  LoadImages();
  Init();
}

void wxMTReportFrame::LoadImages()
{
//  #ifdef LOG
//  wxString msg;
//  msg.Printf("appdir - %s\n",wxGetApp().AppDir);
//  WRITELOG(msg);
//  #endif // LOG
  try
    {
      wxString appdir = wxGetApp().AppDir;
      ImageList1->Add(wxImage(appdir + ERRR_FILE_PATH ));

      //� ������������ � ����� eRepElementType
      ImageList1->Add(wxImage(appdir + LINE_FILE_PATH ));
      ImageList1->Add(wxImage(appdir + RECT_FILE_PATH ));
      ImageList1->Add(wxImage(appdir + TEXT_FILE_PATH ));
      ImageList1->Add(wxImage(appdir + IMAG_FILE_PATH ));

      ImageList1->Add(wxImage(appdir + ROOT_FILE_PATH )); //root - �������������
      ImageList1->Add(wxImage(appdir + PAGE_FILE_PATH )); //������ ���������

      //������
      BBtn_Add->SetBitmap(wxImage(appdir + ADDB_FILE_PATH));
      BBtn_Del->SetBitmap(wxImage(appdir + DELB_FILE_PATH));
    }
  catch(...) {}
}

void wxMTReportFrame::onChangePropertyValue(wxCommandEvent& ev)
{
  ShowPropertyGrid();
  RepaintPane();
  ProjectModified();
}

void wxMTReportFrame::onChangeSelectElement(wxCommandEvent& ev)
{
  Element* el = reinterpret_cast<Element*>(ev.GetClientData());
  if(el==NULL)
    {
      return;
    }

  wxTreeItemId treeitem_id = TreeCtrl1->GetFirstVisibleItem();
  if(!treeitem_id.IsOk())
    {
      return;
    }
  RepTreeData* dt = (RepTreeData*)GetTreeItemData(treeitem_id);
  if(dt!=NULL && dt->id==el->id)
    {
      TreeCtrl1->SelectItem(treeitem_id);
      return;
    }

  for(int f=0; f<TreeCtrl1->GetCount(); ++f)
    {
      treeitem_id = TreeCtrl1->GetNextVisible(treeitem_id);
      if(!treeitem_id.IsOk())
        {
          return;
        }
      dt = (RepTreeData*)GetTreeItemData(treeitem_id);
      if(dt==NULL || dt->id!=el->id)
        {
          continue;
        }
      TreeCtrl1->SelectItem(treeitem_id);
      break;
    }
}
void wxMTReportFrame::ProcessCmdLine()
{
  wxApp* app = &wxGetApp();
  if(app->argc>1)
    {
      wxString openfile = app->argv.GetArguments().Item(1);
      if(!openfile.IsEmpty() && wxFile::Exists(openfile))
        {
          this->LoadProject(openfile);
        }
    }
}
void wxMTReportFrame::Init()
{
  if(SingleInstanceChecker1.IsAnotherRunning())
    {
      Close();
    }

  repproj_=new RepProj();
  reppane->repproj=repproj_;
  propmngr->EnableCategories(true);

  ElementsOnTree();
  MakePropertyGrid();
  RefreshMainMenuLabels();
  RefreshBtnHint();

  RepaintPane();

  StatusBar1->SetLabel(EMPTY_PROJECT_NAME);
  TreeCtrl1->SetItemText( TreeCtrl1->GetRootItem(), EMPTY_PROJECT_NAME);

  this->SetDropTarget(new FileDrop(this));
  ProcessCmdLine();
}
void wxMTReportFrame::RefreshMainMenuLabels()
{
  MenuBar1->SetLabel(MFP_Load     , MAINMENU_LOAD);
  MenuBar1->SetLabel(MFP_SAVE     , MAINMENU_SAVE);
  MenuBar1->SetLabel(MF_PROJECT   , MAINMENU_PROJECT);
  MenuBar1->SetLabel(MF_PRINT     , MAINMENU_PRINT);
  MenuBar1->SetLabel(MF_QUIT      , MAINMENU_QUIT);
  MenuBar1->SetLabel(MFT_SETTINGS , MAINMENU_SETTINGS);
  MenuBar1->SetLabel(MFT_VARS , PGPAGE_VARS);
  MenuBar1->SetLabel(MH_ABOUT     , MAINMENU_ABOUT);

  MenuBar1->SetMenuLabel(0,MAINMENU_FILE);
  MenuBar1->SetMenuLabel(1,MAINMENU_TOOLS);
  MenuBar1->SetMenuLabel(2,MAINMENU_HELP);
}
void wxMTReportFrame::RefreshBtnHint()
{
  BBtn_Add->SetToolTip(BBTN_ADD_HINT);
  BBtn_Del->SetToolTip(BBTN_DEL_HINT);
}
void wxMTReportFrame::ElementsOnTree()
{
  wxTreeItemId treeitem_selected_after_load;
  TreeCtrl1->DeleteAllItems();
  treeitem_selected = TreeCtrl1->AddRoot(_T("+"),ImageList1->GetImageCount()-2);
  treeitem_selected_after_load = treeitem_selected;
  for(long f=0; f<repproj_->docs.GetCount(); f++)
    {
      Doc* doc = repproj_->docs.Get(f);
      if(doc==NULL)
        {
          continue;
        }
      RepTreeData* dt = new RepTreeData();
      treeitem_selected = TreeCtrl1->AppendItem(treeitem_selected,doc->GetName(),ImageList1->GetImageCount()-1,-1, dt );
      dt->type = EL_PAGE;
      dt->id=doc->id;
      if(repproj_->selected_item==doc->id)
        {
          treeitem_selected_after_load = treeitem_selected;
        }

      for(long j=0; j<doc->elems.GetCount(); j++)
        {
          Element* el = doc->elems.Get(j);
          if(el==NULL)
            {
              continue;
            }
          RepTreeData* tdat = new RepTreeData();
          wxTreeItemId item = TreeCtrl1->AppendItem(treeitem_selected,el->GetName(),el->GetType(),-1,tdat);
          tdat->type = el->GetType();
          tdat->id = el->id;

          if(repproj_->selected_item==el->id)
            {
              treeitem_selected_after_load = item;
            }
        }
      treeitem_selected=TreeCtrl1->GetRootItem();
    }
  treeitem_selected = treeitem_selected_after_load;
}
wxString wxMTReportFrame::ShowFileDialog(bool is_save)
{
  wxString filename=wxEmptyString;

  wxFileDialog* dlg = NULL;
  if(is_save==false)
    {
      wxString prompt = _("Open Project");
      dlg = new wxFileDialog(this,prompt,wxEmptyString,wxEmptyString,MTREP_PROJFILTER,wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    }
  else if(is_save==true)
    {
      wxString prompt = _("Save Project");
      dlg = new wxFileDialog(this,prompt,wxEmptyString,wxEmptyString,MTREP_PROJFILTER,wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
    }
  dlg->SetFilterIndex(0);
  if(dlg->ShowModal()==wxID_OK)
    {
      filename = dlg->GetPath();
    }
  delete dlg;

#ifdef LOG
  if(filename!=wxEmptyString)
    {
      wxString msg;
      if(is_save==false)
        {
          msg.Printf("Selected file to load: %s \n",filename);
        }
      else if(is_save==true)
        {
          msg.Printf("Selected file to save: %s \n",filename);
        }
      WRITELOG(msg);
    }
#endif // LOG

  return filename;
}
//=======================================================================
// ��������� �������� ����
void wxMTReportFrame::OnQuit(wxCommandEvent& event)
{
  onCloseFrame();
}
void wxMTReportFrame::OnAbout(wxCommandEvent& event)
{
  AboutDialog dlg(this);
  dlg.ShowModal();
}
void wxMTReportFrame::OnMenuItemPrint(wxCommandEvent& event)
{
  RepTreeData* dt = GetTreeItemData(treeitem_selected);
  if(!dt)
    {
      wxMessageBox(_("Page not selected"));
      return ;
    }
  if(dt->type!=EL_PAGE)
    {
      dt=GetTreeItemData(treeitem_selected,true);
    }
  if(dt->type!=EL_PAGE)
    {
      return;
    }

  Doc* doc = repproj_->docs.Get(dt->id,false);
  if(doc==NULL)
    {
      return;
    }

  if(doc->sizemm.GetHeight()==0 || doc->sizemm.GetWidth()==0)
    {
      wxMessageBox(_("Page size error. Width and Height must be more than 0"));
      return;
    }

  PropertyData* pd =  doc->prop.GetProperty(PROP_VISIBLE_ID);
  if(pd==NULL)
    {
      return;
    }

  if(pd->value.GetLong()&REPORT==0)
    {
      wxMessageBox(_("Page on report is invisible"));
      return;
    }

  RepPrinter* view = new RepPrinter(REPORT,NULL);
  if(!view->SetRepProjectObject(repproj_, dt->id))
    {
      return;
    }

  doc->print_dt.SetPaperId(wxPAPER_NONE);
  doc->print_dt.SetPaperSize(doc->print_dt.GetPaperSize() );

#ifdef LOG
  wxString msg;
  msg.Printf("Preview. Paper size [%d x %d]\n", doc->print_dt.GetPaperSize().GetWidth(),doc->print_dt.GetPaperSize().GetHeight());
  WRITELOG(msg);
#endif

  wxPrintPreview* preview = new wxPrintPreview(view,NULL,&doc->print_dt);
  wxPreviewFrame* frame = new wxPreviewFrame(preview, this, _("Preview"), wxPoint(100, 100), wxSize(600, 650));
  frame->Centre(wxBOTH);
  frame->Initialize();
  frame->Show(true);
}

void wxMTReportFrame::LoadProject(wxString filepath)
{
  if(repproj_->LoadProject(filepath))
    {
      SetVariablesProperty(repproj_->vars);
      ElementsOnTree();

      StatusBar1->SetLabel(FILE_LOADED_HEAD_HINT+repproj_->ProjFile);
      TreeCtrl1->SetItemText( TreeCtrl1->GetRootItem(), repproj_->ProjFile);
      TreeCtrl1->SelectItem(treeitem_selected);
    }
}

void wxMTReportFrame::OnMenuItemLoadProject(wxCommandEvent& event)
{
  wxString fname = ShowFileDialog(false);
  if(fname == wxEmptyString)
    {
      return;
    }
  LoadProject(fname);
}
void wxMTReportFrame::OnMenuItemSaveProject(wxCommandEvent& event)
{
  wxString fname = ShowFileDialog(true);
  if(fname == wxEmptyString)
    {
      return;
    }
  repproj_->SaveProject(fname);
}
void wxMTReportFrame::OnMenuSettingsSelected(wxCommandEvent& event)
{
  SettingsDialog dlg(this);
  if(dlg.ShowModal()==wxID_APPLY)
    {
      PropertiesConvertLocale();
      ShowPropertyGrid();
      RefreshMainMenuLabels();
      RefreshBtnHint();
    }
}
void wxMTReportFrame::OnMenuVariablesSelected(wxCommandEvent& event)
{
  VarsDialog dlg(this);
  dlg.SetVars(repproj_->vars.GetVars());
  if(dlg.ShowModal()==wxID_APPLY)
    {
      repproj_->vars.SetVars(dlg.GetVars());
    }
}
//=======================================================================

//=======================================================================
// ������ � ������� ��������
bool wxMTReportFrame::IsTreeRootSelected(RepTreeData* sel_dt)
{
  RepProperties* props= NULL;
  if(sel_dt==NULL|| sel_dt->type==EL_NULL || treeitem_selected==TreeCtrl1->GetRootItem())
    {
      if(treeitem_selected==TreeCtrl1->GetRootItem())
        {
          props = &(repproj_->prop);
        }
      GetPropertiesValue(props,false);
      RepaintPane();
      return true;
    }
  return false;
}
RepTreeData* wxMTReportFrame::GetTreeItemData(wxTreeItemId id, bool need_parent)
{
  if(id == TreeCtrl1->GetRootItem())
    {
      return NULL;
    }

  if(!need_parent)
    {
      return (RepTreeData*)TreeCtrl1->GetItemData(id);
    }
  else
    {
      return (RepTreeData*)TreeCtrl1->GetItemData(TreeCtrl1->GetItemParent(id));
    }
}
void wxMTReportFrame::OnTreeCtrlSelectionChanged(wxTreeEvent& event)
{
  treeitem_selected = TreeCtrl1->GetFocusedItem();
  if(!treeitem_selected.IsOk())
    {
      return;
    }

  RepTreeData* sel_dt = (RepTreeData*)TreeCtrl1->GetItemData(treeitem_selected);
  if(IsTreeRootSelected(sel_dt))
    {
      return;
    }

#ifdef LOG
  wxString msg;
  msg.Printf("Selected: type=%d id=%d \n",sel_dt->type,sel_dt->id);
  WRITELOG(msg);
#endif // LOG

  wxString el_name;
  wxString doc_name;
  bool can_change_type=false;
  RepProperties* props= NULL;
  Element* el = SelectElement(sel_dt);
  Doc* doc = SelectDoc(sel_dt);

  if(el!=NULL)
    {
      el_name = el->GetName();
      props=&el->prop;
      can_change_type=true;
    }
  if(doc!=NULL)
    {
      doc_name = doc->GetName();
      props=&doc->prop;
      el = doc;
    }
#ifdef LOG
  msg.Printf("[doc=%s] [elem_name=%s]\n",doc_name,el_name);
  WRITELOG(msg);
#endif // LOG

  GetPropertiesValue(props,can_change_type);
  RepaintPane();

  reppane->ScrollToSelectedElement();
}
Element* wxMTReportFrame::SelectElement(RepTreeData* sel_dt)
{
  if(sel_dt->type==EL_PAGE)
    {
      return NULL;
    }

  RepTreeData* parent_dt = GetTreeItemData(treeitem_selected,true);
  Doc* doc = repproj_->docs.Get(parent_dt->id,false);
  if(doc==NULL)
    {
      return NULL;
    }
  Element* el = doc->elems.Get(sel_dt->id,false);
  if(el==NULL)
    {
      return NULL;
    }
  repproj_->selected_item = el->id;
  return el;
}
Doc* wxMTReportFrame::SelectDoc(RepTreeData* sel_dt)
{
  if(sel_dt->type!=EL_PAGE)
    {
      return NULL;
    }

  Doc* doc = repproj_->docs.Get(sel_dt->id,false);
  if(doc==NULL)
    {
      return NULL;
    }

  repproj_->selected_item = doc->id;
  reppane->SetBackgroundColour(doc->prop.GetProperty(PAPER_BGROUND_ID)->value.GetLong());
  return doc;
}

void wxMTReportFrame::OnButtonAdd(wxCommandEvent& event)
{
  if(TreeCtrl1->IsEmpty())
    {
      return;
    }
  if(treeitem_selected==TreeCtrl1->GetRootItem())
    {
      AddPage();
    }
  else
    {
      AddElement();
    }
  TreeCtrl1->Expand(treeitem_selected);
}
void wxMTReportFrame::AddPage()
{
  if(treeitem_selected==TreeCtrl1->GetRootItem())
    {
      Doc* doc = repproj_->docs.AddNew(EL_PAGE);

      RepTreeData* dt = new RepTreeData();
      dt->type=EL_PAGE;
      dt->id=doc->id;
      TreeCtrl1->AppendItem(treeitem_selected,doc->GetName(),ImageList1->GetImageCount()-1,-1, dt );
      ProjectModified();
      return;
    }
}
void wxMTReportFrame::AddElement()
{
  RepTreeData* dt = GetTreeItemData(treeitem_selected);
  if(dt->type==EL_PAGE)
    {
      Doc* doc = repproj_->docs.Get(dt->id,false);
      if(doc==NULL)
        {
          return;
        }

      Element* el = doc->elems.AddNew(EL_UNKNOWN);

      RepTreeData* dt = new RepTreeData();
      dt->type=EL_UNKNOWN;
      dt->id=el->id;
      TreeCtrl1->AppendItem(treeitem_selected,el->GetName(),-1,-1,dt);
      ProjectModified();
      return;
    }
}
void wxMTReportFrame::OnButtonDel(wxCommandEvent& event)
{
  if(TreeCtrl1->IsEmpty() || treeitem_selected==TreeCtrl1->GetRootItem())
    {
      return;
    }
  RepTreeData* sel_dt = GetTreeItemData(treeitem_selected);
  if(sel_dt==NULL)
    {
      return;
    }


  if(sel_dt->type==EL_PAGE)
    {
      if(!repproj_->docs.Delete(sel_dt->id,false))
        {
          return;
        }
    }
  else if(sel_dt->type!=EL_PAGE)
    {
      RepTreeData* parent_dt = GetTreeItemData(treeitem_selected,true);
      Doc* doc = repproj_->docs.Get(parent_dt->id,false);
      if(doc==NULL)
        {
          return;
        }
      if(!doc->elems.Delete(sel_dt->id,false))
        {
          return;
        }
    }
  wxTreeItemId item_parent = TreeCtrl1->GetItemParent(treeitem_selected);
  TreeCtrl1->Delete(treeitem_selected);

  TreeCtrl1->SelectItem(item_parent);
  treeitem_selected=item_parent;
  ProjectModified();
}
void wxMTReportFrame::OnTreeCtrl1BeginDrag(wxTreeEvent& event)
{
  wxPoint pnt = event.GetPoint();
  wxTreeItemId pos = TreeCtrl1->HitTest(pnt);
  if(pos == TreeCtrl1->GetRootItem())
    {
      return;
    }

  RepTreeData* data = GetTreeItemData(pos);

  if(data->type!=EL_PAGE)
    {
      treeitem_selected=pos;
      event.Allow();
    }
}
void wxMTReportFrame::OnTreeCtrl1EndDrag(wxTreeEvent& event)
{
  //��������� ������ �� ��������, ������� ��������
  RepTreeData* sel_data = GetTreeItemData(treeitem_selected);
  RepTreeData* sel_pardata = GetTreeItemData(treeitem_selected,true);
  Doc* doc = repproj_->docs.Get(sel_pardata->id,false);
  if(doc==NULL)
    {
      return;
    }
  Element* el = doc->elems.Get(sel_data->id,false);
  if(el==NULL)
    {
      return;
    }
  wxString name = el->GetName();

  //���������� ������ �� ��������, �� ������� ��������
  wxPoint pnt = event.GetPoint();
  wxTreeItemId newpos = TreeCtrl1->HitTest(pnt);
  if(!newpos.IsOk())
    {
      return;
    }

  RepTreeData* drop_data = GetTreeItemData(newpos);
  RepTreeData* drop_pardata=NULL;
  Doc* drop_doc=NULL;
  Element* drop_el=NULL;

  //��������� ������ �� ��������, �� ������� ��������
  if(drop_data->type!=EL_PAGE) //�������� �� �������
    {
      drop_pardata = GetTreeItemData(drop_data->GetId(),true);
    }
  else //�������� �� ��������
    {
      drop_pardata = drop_data;
    }
  long indx = 0;
  drop_doc = repproj_->docs.Get(drop_pardata->id,false);
  if(drop_doc==NULL)
    {
      return;
    }
  if(drop_data->type!=EL_PAGE)
    {
      drop_el = drop_doc->elems.Get(drop_data->id,false);
      if(drop_el==NULL)
        {
          return;
        }
      indx=drop_doc->elems.GetIndex(drop_el->id);
      if(indx<0)
        {
          return;
        }
    }

  //��������������� ���������� ��������
  RepTreeData* dt = new RepTreeData();
  dt->type = el->GetType();
  dt->id = el->id;

  if(drop_pardata->id == sel_pardata->id)//�� ��� �� �������� (�����)
    {
      if(!drop_doc->elems.Move(el,indx))
        {
          return;
        }

    }
  else //�� ������ �������� (����������� �� �������)
    {
      Element inselem(*el);
      inselem.id=wxNewId();
      dt->id=inselem.id;
      drop_doc->elems.Insert(inselem,indx);

    }

  TreeCtrl1->Delete(treeitem_selected);
  TreeCtrl1->InsertItem(drop_pardata->GetId(),indx,name,dt->type,-1,dt);

  RepaintPane();
  ProjectModified();
}
//=======================================================================

//=======================================================================
// ������ � ������������ ������
void wxMTReportFrame::RepaintPane()
{
  reppane->SetPageId(-1);
  Doc* doc=NULL;
  long el_id = -1;
  RepTreeData* dt = GetTreeItemData(treeitem_selected);

  if(dt!=NULL && dt->type!=EL_PAGE)
    {
      el_id = dt->id;
      dt = GetTreeItemData(treeitem_selected,true);
    }

  if(dt!=NULL)
    {
      doc=repproj_->docs.Get(dt->id,false);
    }

  if(doc==NULL)
    {
      RefreshRect(reppane->GetRect());
      return;
    }

  SetPageSize(doc);

  reppane->SetPageId(doc->id);
  reppane->ShowElementMarkers(el_id);
  reppane->Repaint();
}

void wxMTReportFrame::SetPageSize(Doc* doc)
{
  if(doc==NULL)
    {
      return;
    }

  double pkf =0;
  double pw = 0;
  double ph = 0;
  PropertyData* prop = doc->prop.GetProperty(PROP_SIZEMETER_ID);
  if(prop!=NULL)
    {
      pkf = prop->value.GetDouble();
    }
  prop = doc->prop.GetProperty(PROP_WIDTH_ID);
  if(prop!=NULL)
    {
      pw = prop->value.GetDouble();
    }
  prop = doc->prop.GetProperty(PROP_HEIGHT_ID);
  if(prop!=NULL)
    {
      ph = prop->value.GetDouble();
    }
  doc->sizemm.Set((int)(pw*pkf) ,(int)(ph*pkf));
}
//=======================================================================

//=======================================================================
// ������ �� ���������� ���������� ������� ������
void wxMTReportFrame::PropertiesConvertLocale()
{
  for(int d=0; d<repproj_->docs.GetCount(); ++d)
    {
      Doc* doc = repproj_->docs.Get(d,true);
      if(!doc)
        {
          continue;
        }
      doc->prop.ConvertLocale();
      for(int e=0; e<doc->elems.GetCount(); ++e)
        {
          Element* el = doc->elems.Get(e,true);
          if(!el)
            {
              continue;
            }
          el->prop.ConvertLocale();
        }
    }
}
void wxMTReportFrame::ShowPropertyGrid()
{
  MakePropertyGrid();

  RepTreeData* dt = GetTreeItemData(treeitem_selected);
  RepProperties* properties = NULL;
  Doc* doc=NULL;
  Element* el = NULL;
  bool is_element = false;

  if(dt==NULL)
    {
      return;
    }
  else if(dt->type==EL_PAGE)
    {
      doc = repproj_->docs.Get(dt->id,false);
      if(doc==NULL)
        {
          return;
        }
      properties=&doc->prop;
      el= doc;
    }

  if (dt->type!=EL_PAGE)
    {
      RepTreeData* pdt = GetTreeItemData(treeitem_selected,true);
      doc = repproj_->docs.Get(pdt->id,false);
      if(doc==NULL)
        {
          return;
        }
      el = doc->elems.Get(dt->id,false);
      if(el==NULL)
        {
          return;
        }
      properties = &el->prop;
      is_element=true;
    }

  GetPropertiesValue(properties,is_element);
  reppane->ShowElementMarkers(el->id);
}
void wxMTReportFrame::MakePropertyGrid()
{
  propmngr->Clear();
  propmngr->AddPage(PropertyNames::GetNameByID(PGPAGE_PROP_ID));
  propmngr->Bind(wxEVT_PG_CHANGED,&wxMTReportFrame::OnPropertyChanged,this,ID_PROPMNGR1);
}
void wxMTReportFrame::OnPropertyChanged(wxPropertyGridEvent& event)
{
  if(event.GetPropertyName()==PropertyNames::GetNameByID(PROP_VARSLIST_ID))
    {
      repproj_->vars.SetVars(event.GetValue().GetString());
      SetVariablesProperty(repproj_->vars);
    }
  else
    {
      SetPropertiesValue(event.GetProperty());
    }
  ProjectModified();
}
void wxMTReportFrame::SetVariablesProperty(RepVars vars)
{
  wxVector<Var> v = vars.GetVars();
  if(v.empty())
    {
      return;
    }

  wxPGProperty* prop = propmngr->GetPropertyByName(PropertyNames::GetNameByID(PROP_VARSLIST_ID));
  if(prop==NULL)
    {
      return;
    }

  wxString val=_T("");
  wxVector<Var>::iterator iter;
  for(iter=v.begin(); iter!=v.end(); iter++)
    {
      val+=(*iter).name+_T("\\n");
    }
  prop->SetValue(val);
}
void wxMTReportFrame::GetPropertiesValue(RepProperties* props, bool can_change_type,bool refresh)
{
  wxPropertyGridPage* pg = propmngr->GetPage(PropertyNames::GetNameByID(PGPAGE_PROP_ID));
  if(pg!=NULL && !refresh)
    {
      pg->Clear();
    }
  if(props==NULL)
    {
      return;
    }

  for(int f=0; f<props->GetCount(); f++)
    {
      PropertyData* pd = props->GetProperty(f);
//#ifdef LOG
//    wxPrintf("property:%d pid=%d type=%d choise_id=%d\n",f,pd->nam_id,pd->type,pd->choise_id);
//#endif // LOG
      if(pd->nam_id==PROP_TYPE_ID && !can_change_type)
        {
          continue;
        }
      wxPGProperty* p = NULL;
      if(!refresh)
        {
          p = CreateProperty(pd);
        }
      else
        {
          p = pg->GetProperty(PropertyNames::GetNameByID(pd->nam_id));
        }

      if(p!=NULL && !refresh)
        {
          wxPGProperty* pc= pg->GetProperty( PropertyNames::GetNameByID(pd->cat_id) );
          if(pc==NULL)
            {
              pc = new wxPropertyCategory(PropertyNames::GetNameByID(pd->cat_id),wxPG_LABEL);
              pg->Append(pc);
            }
          if(pc->GetPropertyByName(PropertyNames::GetNameByID(pd->nam_id))==NULL)
            {
              pc->AppendChild(p);
            }
        }
      else if(p!=NULL && refresh)
        {
          p->SetValue(pd->value);
        }
    }
}
void wxMTReportFrame::SetPropertiesValue(wxPGProperty* property)
{
  wxString pname = property->GetName(); //��� ����������� ��������
  Property_ID prop_id = (Property_ID)PropertyNames::GetIDByName(pname);

  PropertyData* p=NULL;
  Doc* doc=NULL;
  Element* el = NULL;
  RepTreeData* dt = GetTreeItemData(treeitem_selected);
  RepProperties* properties = NULL;
  bool is_element=false;
  bool refresh=true;
  bool table_changed=false;
  if(dt==NULL)
    {
      if(treeitem_selected==TreeCtrl1->GetRootItem())
        {
          p=repproj_->prop.GetProperty(prop_id);
          p->value = property->GetValue();
        }
      return;
    }

  if(dt->type==EL_PAGE)
    {
      doc = repproj_->docs.Get(dt->id,false);
      if(doc==NULL)
        {
          return;
        }

      p = doc->prop.GetProperty(prop_id);
      if(p==NULL)
        {
          return;
        }

      if(p->nam_id == PROP_SIZEMETER_ID)
        {
          doc->ChangeSizeScale(property->GetValue());
          table_changed=true;
          refresh=false;
        }
      if(p->nam_id == PROP_PAGESIZE_ID)
        {
          doc->ChangePaperSize(property->GetValue().GetLong());
          table_changed=true;
          refresh=false;
        }
      if(p->nam_id == PROP_PAGEORIENT_ID)
        {
          doc->ChangePaperOrient(property->GetValue().GetLong());
          table_changed=true;
          refresh=false;
        }

      p->value = property->GetValue();

      if(p->nam_id == PROP_NAME_ID)
        {
          TreeCtrl1->SetItemText(treeitem_selected,doc->GetName());
        }
      if(p->type==PR_COLOR)
        {
          p->SetColour(property->GetValue());
        }
      properties=&doc->prop;
      el=doc;
    }

  if (dt->type!=EL_PAGE)
    {
      RepTreeData* pdt = GetTreeItemData(treeitem_selected,true);
      doc = repproj_->docs.Get(pdt->id,false);
      if(doc==NULL)
        {
          return;
        }
      el = doc->elems.Get(dt->id,false);
      if(el==NULL)
        {
          return;
        }
      p = el->prop.GetProperty(prop_id);
      if(p==NULL)
        {
          return;
        }

      if(prop_id == PROP_TYPE_ID)
        {
          eRepElementType type = (eRepElementType)property->GetValue().GetAny().As<long>();
          el->SetType(type);
          dt->type=type;
          table_changed=true;
          refresh=false;
        }

      p->value = property->GetValue();

      if(prop_id==PROP_FONT_ID)
        {
          p->value = property->GetValue().GetAny().As<wxFont>().GetNativeFontInfoDesc();
        }
      if(p->type==PR_COLOR)
        {
          p->SetColour(property->GetValue());
        }
      if(p->nam_id == PROP_NAME_ID)
        {
          TreeCtrl1->SetItemText(treeitem_selected,el->GetName());
        }
      properties = &el->prop;
      is_element=true;
      TreeCtrl1->SetItemImage(treeitem_selected,(int)dt->type);
    }

  if(table_changed)
    {
      GetPropertiesValue(properties,is_element,refresh);
    }
  RepaintPane();
}
wxPGProperty* wxMTReportFrame::CreateProperty(PropertyData* p)
{
  if(p==NULL)
  {
    return NULL;
  }
  wxPGProperty* pgp = NULL;
  wxString name = PropertyNames::GetNameByID(p->nam_id);
  switch(p->type)
    {
    case PR_STRING:
    {
      pgp=new wxLongStringProperty(name,wxPG_LABEL,p->value.GetString());
      break;
    }
    case PR_INTEGER:
      pgp=new wxIntProperty(name,wxPG_LABEL,p->value.GetLong());
      break;
    case PR_FLOAT:
      pgp=new wxFloatProperty(name,wxPG_LABEL,p->value.GetDouble());
      break;
    case PR_COLOR:
    {
      wxColour c;
      p->GetColour(c);
      pgp=new wxColourProperty(name,wxPG_LABEL,c);
      break;
    }
    case PR_SWITCH:
    {
      wxPGChoices* choices = p->GetChoices();
      pgp=new wxEnumProperty(name,wxPG_LABEL,*choices,p->value.GetLong());
      break;
    }
    case PR_LINETYPE:
    {
      wxPGChoices* choices = p->GetChoices();
      pgp = new PenStyleComboProperty(name,wxPG_LABEL,*choices,p->value.GetLong());
      break;
    }
    case PR_BRUSHTYPE:
    {
      wxPGChoices* choices = p->GetChoices();
      pgp = new FillStyleComboProperty(name,wxPG_LABEL,*choices,p->value.GetLong());
      break;
    }
    case PR_FONTSELECTOR:
    {
      wxFont fnt(p->value.GetString());
      pgp=new wxFontProperty(name,wxPG_LABEL,fnt);
      break;
    }
    case PR_FILESELECTOR:
    case PR_IMGFILESELECTOR:
    {
      wxString fpath = p->value.GetString();
      pgp=new wxImageFileProperty(name,wxPG_LABEL,fpath);
      if(!wxFile::Exists(fpath))
        {
          TreeCtrl1->SetItemImage(treeitem_selected,0);
        }
      break;
    }
    case PR_BOOL:
    {
      pgp=new wxBoolProperty(name,wxPG_LABEL,p->value.GetBool());
      pgp->SetAttribute(wxPG_BOOL_USE_CHECKBOX , true);
      break;
    }
    case PR_BOOLARRAY:
    {
      wxPGChoices* choices = p->GetChoices();
      if(choices == NULL)
      {
        return NULL;
      }
      for(unsigned int f=0; f<choices->GetCount(); ++f)
        {
          choices->Item(f).SetValue(1<<f); //�������� �� �����
        }
      pgp=new wxFlagsProperty(name,wxPG_LABEL,*choices,p->value.GetLong());
      pgp->SetAttribute(wxPG_BOOL_USE_CHECKBOX , true);
      break;
    }
    }
  pgp->Enable(p->is_enabled);
  return pgp;
}
//=======================================================================

void wxMTReportFrame::OnClose(wxCloseEvent& event)
{
  onCloseFrame();
}
void wxMTReportFrame::onCloseFrame()
{
  if(repproj_->IsModify)
    {
      const wxString question = _("Need to Save the Project?");
      wxArrayString choices;
      choices.Add(_("Yes")); //0
      choices.Add(_("No"));
      void** cltdata = NULL;
      wxSingleChoiceDialog chdlg(this, question, MAINMENU_QUIT, choices, cltdata, wxCENTRE | wxRESIZE_BORDER, wxDefaultPosition);
      chdlg.ShowModal();
      if(chdlg.GetSelection()==0)
        {
          if(repproj_->ProjFile==wxEmptyString)
            {
              wxCommandEvent evt(wxEVT_COMMAND_MENU_SELECTED,MFP_SAVE);
              ProcessWindowEvent(evt);
            }
          else
            {
              repproj_->SaveProject(repproj_->ProjFile);
            }
        }
    }
  Destroy();
}
void wxMTReportFrame::ProjectModified()
{
  if(repproj_->IsModify)
    {
      return;
    }
  repproj_->IsModify=true;
  StatusBar1->SetLabel(StatusBar1->GetLabel()+_T("*"));
}

