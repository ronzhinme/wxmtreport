/***************************************************************
 * Name:      wxMTReportMain.h
 * Purpose:   Defines Application Frame
 * Author:    Ronzhin Mikhail ()
 * Created:   2013-08-22
 * Copyright: Ronzhin Mikhail ()
 * License:
 **************************************************************/

#ifndef WXMTREPORTMAIN_H
#define WXMTREPORTMAIN_H

//(*Headers(wxMTReportFrame)
#include <wx/treectrl.h>
#include <wx/sizer.h>
#include <wx/app.h>
#include <wx/menu.h>
#include <wx/splitter.h>
#include "RepPane.h"
#include <wx/propgrid/manager.h>
#include <wx/panel.h>
#include <wx/snglinst.h>
#include <wx/bmpbuttn.h>
#include <wx/imaglist.h>
#include <wx/utils.h>
#include <wx/frame.h>
#include <wx/statusbr.h>
//*)
#include <wx/richtext/richtextprint.h>
#include <wx/propgrid/manager.h>
#include <wx/dc.h>

#include "SettingsDialog.h"
#include "AboutDialog.h"
#include "VarsDialog.h"

#include "./ObjModelLib/defines.h"

using namespace ElementModel;

class wxMTReportFrame: public wxFrame
{
public:

  wxMTReportFrame(wxWindow* parent,wxWindowID id = -1);
  virtual ~wxMTReportFrame();
  void LoadProject(wxString filepath);
private:

  //(*Handlers(wxMTReportFrame)
  void OnQuit(wxCommandEvent& event);
  void OnAbout(wxCommandEvent& event);
  //  void OnRepPanePaint(wxPaintEvent& event);
  void OnMenuItemPrint(wxCommandEvent& event);
  void OnMenuItemLoadProject(wxCommandEvent& event);
  void OnMenuItemSaveProject(wxCommandEvent& event);
  void OnTreeCtrlSelectionChanged(wxTreeEvent& event);
  void OnButtonAdd(wxCommandEvent& event);
  void OnButtonDel(wxCommandEvent& event);
  void OnpropmngrKeyDown(wxKeyEvent& event);
  void OnTreeCtrl1BeginDrag(wxTreeEvent& event);
  void OnTreeCtrl1EndDrag(wxTreeEvent& event);
  void OnTreeCtrl1SelectionChanging(wxTreeEvent& event);
  void OnMenuSettingsSelected(wxCommandEvent& event);
  void OnClose(wxCloseEvent& event);
  void OnMenuVariablesSelected(wxCommandEvent& event);
  //*)

  //(*Identifiers(wxMTReportFrame)
  static const long ID_REPPANE;
  static const long ID_PANEL1;
  static const long ID_BITMAPBUTTON1;
  static const long ID_BITMAPBUTTON2;
  static const long ID_TREECTRL1;
  static const long ID_PANEL3;
  static const long ID_PROPMNGR1;
  static const long ID_PANEL4;
  static const long ID_SPLITTERWINDOW2;
  static const long ID_PANEL2;
  static const long ID_SPLITTERWINDOW1;
  static const long MFP_Load;
  static const long MFP_SAVE;
  static const long MF_PROJECT;
  static const long MF_PRINT;
  static const long MF_QUIT;
  static const long MFT_SETTINGS;
  static const long MFT_VARS;
  static const long MH_ABOUT;
  static const long ID_STATUSBAR1;
  //*)

  //(*Declarations(wxMTReportFrame)
  wxMenuItem* MenuItem8;
  wxMenuItem* MenuItem7;
  wxMenuItem* MenuItem5;
  wxPanel* Panel4;
  wxMenuItem* MenuItem2;
  wxMenu* Menu3;
  wxSplitterWindow* SplitterWindow2;
  wxMenuItem* MenuItem1;
  wxImageList* ImageList1;
  wxMenu* MenuItem4;
  wxPanel* Panel1;
  RepPane* reppane;
  wxMenu* Menu1;
  wxPanel* Panel3;
  wxSingleInstanceChecker SingleInstanceChecker1;
  wxMenuItem* MenuItem3;
  wxStatusBar* StatusBar1;
  wxPropertyGridManager* propmngr;
  wxMenuItem* MenuItem6;
  wxTreeCtrl* TreeCtrl1;
  wxMenuBar* MenuBar1;
  wxPanel* Panel2;
  wxSplitterWindow* SplitterWindow1;
  wxMenu* Menu2;
  wxBitmapButton* BBtn_Del;
  wxBitmapButton* BBtn_Add;
  //*)
  void LoadImages();
  bool IsTreeRootSelected(RepTreeData* sel_dt);
  RepProj* repproj_;
  wxTreeItemId treeitem_selected;

  void RefreshMainMenuLabels();
  void RefreshBtnHint();
  Element* SelectElement(RepTreeData* sel_dt);
  Doc* SelectDoc(RepTreeData* sel_dt);

  void AddPage();
  void AddElement();
  wxString ShowFileDialog(bool is_save);
  void Init();
  void ElementsOnTree();
  void MakePropertyGrid();
  void ShowPropertyGrid();
  void SetPageSize(Doc* doc);
  void RepaintPane();
  void OnRepPanePaint(wxPaintEvent& event);
  void OnPropertyChanged(wxPropertyGridEvent& event);
  void SetPropertiesValue(wxPGProperty* property);
  void GetPropertiesValue(RepProperties* props, bool can_change_type=true, bool refresh=false);
  void SetVariablesProperty(RepVars vars);
  wxPGProperty* CreateProperty(PropertyData* p);
  RepTreeData* GetTreeItemData(wxTreeItemId id, bool need_parent=false);
  void PropertiesConvertLocale();
  void onChangePropertyValue(wxCommandEvent& ev);
  void onChangeSelectElement(wxCommandEvent& ev);
  void onCloseFrame();
  void ProjectModified();
  void ProcessCmdLine();
  DECLARE_EVENT_TABLE()
};
#endif // WXMTREPORTMAIN_H
